package com.rapan.bskproject2;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ImageExtractorTests {

    @Test
    public void imageExtractingTest(){
        File file = new File(Paths.get("test.png").toUri());
        byte[] byteImage = null;
        try {
            byteImage = ImageExtractor.extractBytesFromImage(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedImage image = ImageExtractor.extractImageFromBytes(byteImage);
        assertNotEquals(image, null);
    }

    @Test
    public void imageExtractingTest1() {
        File file = new File(Paths.get("test.png").toUri());
        byte[] byteImage = null;
        try {
            byteImage = ImageExtractor.extractBytesFromImage(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
