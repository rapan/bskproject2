package com.rapan.bskproject2;

import com.rapan.bskproject2.entities.Entertainer;
import com.rapan.bskproject2.entities.Zone;
import com.rapan.bskproject2.repositories.EntertainerRepository;
import com.rapan.bskproject2.repositories.ZoneRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private EntertainerRepository entertainerRepository;
    private ZoneRepository zoneRepository;

    public Bootstrap(EntertainerRepository entertainerRepository, ZoneRepository zoneRepository) {
        this.entertainerRepository = entertainerRepository;
        this.zoneRepository = zoneRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        fillDatabase();
    }

    private void fillDatabase(){
        Zone zone = new Zone("Strefa1");
        Zone zone2 = new Zone("Strefa2");
        File image = new File(Paths.get("test.png").toUri());

        try {
            byte [] imageBytes = ImageExtractor.extractBytesFromImage(image);
            Entertainer e1 = new Entertainer("Atrakcja1", zone, 120, imageBytes);
            Entertainer e2 = new Entertainer("Atrakcja2", zone2, 100, imageBytes);
            Entertainer e3 = new Entertainer("Atrakcja3", zone, 120, imageBytes);
            Entertainer e4 = new Entertainer("Atrakcja4", zone2, 100, imageBytes);
            Entertainer e5 = new Entertainer("Atrakcja5", zone, 120, imageBytes);
            Entertainer e6 = new Entertainer("Atrakcja6", zone2, 100, imageBytes);
            zone.getEntertainers().add(e1);
            zone2.getEntertainers().add(e2);

            zoneRepository.save(zone);
            zoneRepository.save(zone2);
            entertainerRepository.saveAll(Arrays.asList(e1,e2,e3,e4,e5,e6));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
