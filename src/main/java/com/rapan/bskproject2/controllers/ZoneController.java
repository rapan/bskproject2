package com.rapan.bskproject2.controllers;

import com.rapan.bskproject2.ImageExtractor;
import com.rapan.bskproject2.entities.Entertainer;
import com.rapan.bskproject2.entities.Zone;
import com.rapan.bskproject2.repositories.EntertainerRepository;
import com.rapan.bskproject2.repositories.ZoneRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Controller
public class ZoneController {

    private ZoneRepository zoneRepository;
    private EntertainerRepository entertainerRepository;

    public ZoneController(ZoneRepository zoneRepository, EntertainerRepository entertainerRepository) {
        this.zoneRepository = zoneRepository;
        this.entertainerRepository = entertainerRepository;
    }

    @RequestMapping("/strefy")
    public String setSecondPage(Model model){

        model.addAttribute("allZones", zoneRepository.findAll());
        return "strefy";
    }

    @RequestMapping("/atrakcje")
    public String setEntertainersPage(@RequestParam int zoneId, Model model){
        model.addAttribute("zoneEntertainers", entertainerRepository.findByZone(zoneId));
        model.addAttribute("zoneName", zoneRepository.findById(zoneId).getName());
        return "atrakcje";
    }
}
