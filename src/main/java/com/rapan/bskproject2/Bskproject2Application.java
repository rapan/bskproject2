package com.rapan.bskproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bskproject2Application {

	public static void main(String[] args) {
		SpringApplication.run(Bskproject2Application.class, args);
	}
}
