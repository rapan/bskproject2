package com.rapan.bskproject2.repositories;

import com.rapan.bskproject2.entities.Zone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ZoneRepository extends CrudRepository<Zone, Long> {
    Zone findById(@Param("zoneId") int id);
}
