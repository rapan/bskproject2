package com.rapan.bskproject2.repositories;

import com.rapan.bskproject2.entities.Entertainer;
import com.rapan.bskproject2.entities.Zone;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntertainerRepository extends CrudRepository<Entertainer, Long> {
    @Query("select e from Entertainer e join e.zone z where z.id = :zoneId")
    List<Entertainer> findByZone(@Param("zoneId") int zoneId);
}
