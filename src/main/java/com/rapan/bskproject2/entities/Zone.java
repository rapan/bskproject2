package com.rapan.bskproject2.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Zone {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;

    @OneToMany(mappedBy = "zone")
    private Set<Entertainer> entertainers = new HashSet<>();

    public Zone(String name) {
        this.name = name;
    }
    public Zone() { }

    public Set<Entertainer> getEntertainers() {
        return entertainers;
    }

    public int getId() { return id; }
    public String getName() { return name; }
}