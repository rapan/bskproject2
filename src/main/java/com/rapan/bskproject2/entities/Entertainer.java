package com.rapan.bskproject2.entities;

import javax.persistence.*;

@Entity
public class Entertainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int minHeight;
    @ManyToOne
    private Zone zone;

    @Lob
    private byte[] image;

    public Entertainer(String name, Zone zone, int minHeight, byte[] image) {
        this.name = name;
        this.zone = zone;
        this.minHeight = minHeight;
        this.image = image;
    }

    public Entertainer() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public int getMinHeight() {
        return minHeight;
    }

    public void setMinHeight(int minHeight) {
        this.minHeight = minHeight;
    }

    public byte[] getImage() { return image; }

    public void setImage(byte[] image) { this.image = image; }
}
